# \<sangha-responsive-image\>

Display an image with custom size.

```html
<sangha-responsive-image
  img="img uri"
  width=500
  height=500
  alt="your alt"
></sangha-responsive-image>
```

## Development

```bash
# Get dependencies
$ npm install

# Demo site
$ npm start

# Run tests
$ npm test
```
